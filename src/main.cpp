//
// Created by Wilkun on 24.05.2019.
//

#include <View.h>
#include <memory>

int main() {
    auto view1 = std::make_shared<ahe::View>();

    view1->displayMainMenu();

    return 0;
}