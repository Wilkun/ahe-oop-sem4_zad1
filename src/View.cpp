//
// Created by Wilkun on 26.05.2019.
//

#include <functional>
#include <map>
#include <iostream>
#include <View.h>

void ahe::View::displayMainMenu() {
    bool run = true;

    std::map<std::string, std::pair<std::string, std::function<void()> > > main_menu;
    main_menu["1"] = std::make_pair<std::string, std::function<void()> >(
            "Pierwsze menu",
            std::bind(&View::displayMainMenu, this)
    );
//    main_menu["2"] = std::make_pair(
//            "Baza pacjentow.",
////                    []() {std::cout << "Wybrano menu pacjentow. Wyswietlam.";}
//            std::bind(&View::displayPatientMenu, this)
//    );
    main_menu["exit"] = std::make_pair<std::string, std::function<void()> >(
            "Wyjscie z programu.",
            [&run]() {
                run = false;
                std::cout << "Konczymy\n";
            }
    );
//
    std::string choice;
    std::cout << "C++ Project Scaffold: \n";
    do {
        std::cout << "Prosze wybrac akcje i nacisnac ENTER: \n";
        auto it = main_menu.begin();
        while (it != main_menu.end()) {
            std::cout << "[" << (it)->first << "] " << (it++)->second.first << std::endl;
        }

        std::cin >> choice;
        if (main_menu.find(choice) == main_menu.end()) {
            /* item isn't found */
            continue; /* next round */
        }
        std::cout << "\n";
        main_menu[choice].second(); /* executes the function */
    } while (run);
//            std::cout << "Thank you and see you again. \n";
}